using hobu.Data;
using Microsoft.AspNetCore.Mvc;

public class UserController : Controller
{
    private readonly ApplicationDbContext _context;

    public UserController(ApplicationDbContext context)
    {
        _context = context;
    }

    [HttpPost]
    public async Task<IActionResult> Create(Utilisateur personne)
    {
        if (!ModelState.IsValid)
        {
            _context.Utilisateur.Add(personne);
            await _context.SaveChangesAsync();
            return View("~/Views/Home/Index.cshtml");
        }        
        return View(personne);
    }

    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }
}
