# Utiliser une image de base de .NET SDK pour la construction
FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS Base
WORKDIR /app
EXPOSE   8080

# Copier les fichiers du projet et restaurer les dépendances
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src
COPY ["hobu.csproj", "."]
RUN dotnet restore "./hobu.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "./hobu.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "hobu.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "hobu.dll"]