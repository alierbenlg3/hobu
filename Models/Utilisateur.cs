using System.ComponentModel.DataAnnotations;

public class Utilisateur
{
    [Key]
    public int Id { get; set; }  

    [Required(ErrorMessage = "Le nom est requis")]
    public string? Nom { get; set; }  

    [Required(ErrorMessage = "Le prénom est requis")]
    public string? Prenom { get; set; }  

    public string? FichierProjet { get; set; }  
}
