#!/bin/sh

# Attendre que la base de données soit prête
until PGPASSWORD="admin123+" psql -h "db" -U "admin" -d "dbtest" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep   1
done

>&2 echo "Postgres is up - executing command"

# Exécuter les migrations
dotnet ef database update

# Exécuter l'application
exec dotnet hobu.dll
